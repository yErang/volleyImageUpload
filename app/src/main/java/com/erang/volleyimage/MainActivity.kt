package com.erang.volleyimage

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.HashMap


class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var ivPhoto: ImageView
    lateinit var btnChoose: Button
    lateinit var btnUpload: Button
    lateinit var etName: EditText
    val IMG_REQUEST = 1
    lateinit var bitmap: Bitmap
    var pap:JSONObject?= null
    val testUrl = "http://192.168.56.101/upload/imageupload.php"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        btnChoose.setOnClickListener(this)
        btnUpload.setOnClickListener(this)
    }

    fun init() {
        ivPhoto = iv
        btnChoose = choose
        btnUpload = upload
        etName = name


    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.choose -> {
                selectImage()
            }
            R.id.upload -> {
                uploadImage()
            }
        }
    }
    fun selectImage() {
        var intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, IMG_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            val path: Uri? = data.data
            bitmap = MediaStore.Images.Media.getBitmap(contentResolver, path)
            ivPhoto.setImageBitmap(bitmap)
            etName.visibility = View.VISIBLE
        }
    }

    fun uploadImage() {
        val requestQueue =Volley.newRequestQueue(this)
        var image: String = imageToString(bitmap)

        var stringReq: StringRequest = object : StringRequest(Request.Method.POST, testUrl, Response.Listener {response ->
            Log.d("Success",response)
        }, Response.ErrorListener { error ->
            Log.e("Error",error.toString())
        }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["name"] = etName.text.toString()
                params["image"] = image
                return params
            }

        }
        requestQueue.add(stringReq)
    }

    fun imageToString(bitmap : Bitmap) : String{
        var byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream)
        var imgBytes = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(imgBytes,Base64.DEFAULT)
    }
}


